import Vue from "vue";
import App from "./App.vue";
import router from "./router";

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");

//console message code
function printImg(url, scale) {
  scale = scale || 1;
  var img = new Image();

  img.onload = function() {
    var dim = getBox(this.width * scale, this.height * scale);
    console.log(
      "%c" + dim.string,
      dim.style +
        "background: url(" +
        url +
        "); background-size: " +
        this.width * scale +
        "px " +
        this.height * scale +
        "px; color: transparent;background-repeat: no-repeat;"
    );
  };

  img.src = url;
}
function getBox(width, height) {
  return {
    string: "+",
    style:
      "font-size: 1px; padding: " +
      Math.floor(height / 8) +
      "px " +
      Math.floor(width / 2) +
      "px; line-height: " +
      height +
      "px;"
  };
}
let dsrc = document.getElementById("dogtax").src;
console.log("%c!שלום לכם","color:#3eaf7c;-webkit-text-stroke: 1px black;font-size: 5em;");
console.log("%cהיי אני רון חסון המפתח של האתר ממדור פלא","color:#3eaf7c;-webkit-text-stroke: 0px black;font-size: 1.6em;");
console.log("%cאם אתם המפתחים מהעתיד שמנסים להבין את הקוד שרשמתי אני מאחל לכם הרבה בהצלחה ותהנו עם ויו","color:#3eaf7c;-webkit-text-stroke: 0px black;font-size: 1.6em;");
console.info("%cVue "+"%cאתר זה פותח באמצעות","color:#3eaf7c;-webkit-text-stroke: 1px black;font-size: 3em;","color:black;font-size: 1.8em;");
console.info("DOG TAX:")
printImg(dsrc,0.3);
